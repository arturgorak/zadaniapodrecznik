#include "klasy.h"
#include <iostream>

using namespace std;

zad28::zad28(int n){
    head = nullptr;
    this->size = n;
    array = new int[size + 1];
    for(int i = 0; i < n; i++){
        array[i] = 0;
    }
}

void zad28:: insert(int i){
    this->array[i]++;
    node *tmp = new node;
    tmp->value = i;
    tmp->next = this->head;
    this->head = tmp;
}

void zad28:: select(){
    if(this->head != nullptr){
        array[head->value]--;
        node* tmp = head;
        head = head->next;

        delete tmp;

    }
    else{
        cout << "Ciezko usunac cos z pustego zbioru" << endl;
    }
}

void zad28:: search(int i){
    if(i > this->size or i < 1){
        cout << "Liczba spoza przedziału" << endl;
    }
    else if(this->array[i] > 0){
        cout << "Tak, nalezy" << endl;
    }
    else{
        cout << "Nie nalezy" << endl;
    }
}

void zad28:: print(){
    for(int i = 0; i <= size; i++){
        for(int j = 0; j < array[i]; j++){
            cout << i << " ";
        }
    }
    cout << endl;

    node* tmp = this->head;
    if(tmp != nullptr){
        while(tmp != nullptr){
            cout << tmp->value << " ";
            tmp = tmp->next;

        }
        cout << endl;
    }
}

zad29::zad29(int n){
    head = nullptr;
    array = new arrayNode[n];
    size = n;
    for(int i = 0; i < n; i++){
        array[i].head = nullptr;
        array[i].ile = 0;
    }
}

void zad29:: push(int i){
    node2* tmp = new node2;
    tmp->value = i;
    tmp->prev = nullptr;
    if(head != nullptr){
        head->prev = tmp;
    }
    tmp->next = this->head;
    this->head = tmp;
    this->head->nextV = this->array[i].head;
    array[i].head = this->head;
    if(array[i].ile != 0){
        array[i].head->prevV = this->head;
    }
    else{
        this->head->prevV = nullptr;
    }
    array[i].ile++;

}

void zad29:: pop(){
    if(head != nullptr){
        if(head->prevV != nullptr){
            head->prevV->nextV = head->nextV;
        }
        else{
            array[head->value].head = head->nextV;
        }
        if(head->nextV != nullptr){
            head->nextV->prevV = head->prevV;
        }

        if(array[head->value].head == head){
            array[head->value].head = head->nextV;
        }
        array[head->value].ile--;

        if(head->next != nullptr){
            head->next->prev = nullptr;
        }
        node2* tmp = head;
        head = head->next;
        delete tmp;
    }
    else{
        cout << "Nie da sie usunac pierwszego elemntu z pustego ciagu" << endl;
    }

}

void zad29::search(int i){
    if(array[i].ile > 0){
        cout << "Tak, element " << i << " znajduje sie w liscie" << endl;
    }
    else{
        cout << "Nie, element " << i << " nie znajduje sie w liscie" << endl;
    }
}

void zad29::delete_i(int i) {
    if (array[i].ile == 0) {
        cout << "Nie ma takiego elementu" << endl;
    } else {
        node2 *tmp = array[i].head;
        if (tmp->prev != nullptr) {
            tmp->prev->next = tmp->next;
        }
        if (tmp->next != nullptr) {
            tmp->next->prev = tmp->prev;
        }
        if (this->head == tmp) {
            this->head = tmp->next;
        }
        array[i].head = array[i].head->nextV;
        array[i].ile--;

        delete tmp;
    }
}

void zad29:: print(){
    node2* tmp = head;
    while(tmp != nullptr){
        cout << tmp->value << " ";
        tmp = tmp->next;
    }
    cout << endl << endl;


    for(int i = 0; i < size; i++){
        if(array[i].ile != 0){
            cout << i << ":" << array[i].ile << ": ";
            tmp = array[i].head;
            while(tmp != nullptr){
                cout << tmp->value << " ";
                tmp = tmp->nextV;
            }
            cout << endl;
        }
    }

    cout << "----------------------------" << endl;
}

zad30::zad30(){
    this->head = nullptr;
    this->min = nullptr;
}

void zad30::push(int v){
    node3* tmp = new node3;
    tmp->value = v;

    tmp->next = head;
    head = tmp;

    if(this->min == nullptr){
        head->nextV = nullptr;
        min = head;
    }
    else if(min->value > v){
        head->nextV = min;
        min = head;
    }
}

void zad30::pop(){
    if(head != nullptr){
        node3* tmp = head;

        if(head == min){
            min = min->nextV;
        }
        head = head->next;

        delete tmp;
    }
}

void zad30::uptomin(){
    if(min != nullptr){
        head = min->next;
        min = min -> nextV;
    }
}

void zad30::print(){
    node3* tmp = this->head;
    while(tmp != nullptr){
        cout << tmp->value << " ";
        tmp = tmp->next;
    }
    cout << endl;

    tmp = this->min;
    while(tmp != nullptr){
        cout << tmp->value << " ";
        tmp = tmp->nextV;
    }
    cout << endl;
}
