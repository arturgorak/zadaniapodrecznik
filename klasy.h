struct node{
    int value;
    node* next;
};

struct node2{
    int value;
    node2* next;
    node2* prev;
    node2* nextV;
    node2* prevV;
};

struct arrayNode{
    node2* head;
    int ile;
};

struct node3{
    int value;
    node3* next;
    node3* nextV;
};

class zad28{
private:
    node *head;
    int* array;
    int size;
public:
    zad28(int n);
    void insert(int i);
    void select();
    void search(int i);
    void print();
};

class zad29{
private:
    node2* head;
    arrayNode* array;
    int size;
public:
    zad29(int n);
    void push(int i);
    void pop();
    void search(int i);
    void delete_i(int i);
    void print();

};

class zad30{
private:
    node3* head;
    node3* min;
public:
    zad30();
    void push(int v);
    void pop();
    void uptomin();
    void print();
};

