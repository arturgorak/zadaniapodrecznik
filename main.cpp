#include <iostream>
#include "klasy.h"

using namespace std;

int main() {
    zad28 x(15);
    x.insert(12);
    x.insert(15);
    x.insert(12);
    x.select();
    x.select();
    x.print();

    zad29 y(15);
    y.push(5);
    y.push(7);
    y.push(1);
    y.push(5);
    y.push(1);
    y.push(5);
    y.push(1);
    y.push(12);
    y.push(5);
    y.print();
    y.delete_i(7);
    y.delete_i(5);
    y.delete_i(12);
    y.print();
    y.pop();
    y.print();
    y.pop();
    y.print();
    y.pop();
    y.print();
    y.pop();
    y.print();
    y.pop();
    y.print();
    y.push(10);
    y.print();
    y.search(10);
    y.search(0);

    zad30 z;
    z.push(12);
    z.push(11);
    z.push(2);
    z.push(15);
    z.push(30);
    z.push(30);
    z.print();
    z.uptomin();
    z.print();


    return 0;
}
